﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlayEvents : MonoBehaviour
{
    public List<OverlayEvent> events;
    [System.Serializable]
    public class OverlayEvent
    {
        public GameObject eventObj;
        public string text;
        public EventType eventType;
        public enum EventType
        {
            ACTIVATE,
            TEXT
        }
        public void Initialize()
        {
            if (eventType == EventType.ACTIVATE) ActivateObject();
            else WriteText();
        }
        public void ActivateObject()
        {
            eventObj.SetActive(true);
        }
        public void WriteText()
        {

        }
    }
}
