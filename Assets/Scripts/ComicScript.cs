﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ComicScript : MonoBehaviour {
    public List<Node> nodes;
    public OverlayEvents events;
    private int currentNode;
    // Use this for initialization
    void Start() {
        StartCoroutine(MoveToPoint());
    }
    IEnumerator ActivateEvent(int eventNumber)
    {
        yield return new WaitForSeconds(nodes[currentNode].events[eventNumber].time);
        events.events[nodes[currentNode].events[eventNumber].number].Initialize();
    }
    IEnumerator MoveToPoint()
    {
        Vector3 rPoint = nodes[currentNode].point.position - CameraManager.cam.camOffset;
        CameraManager.cam.transform.DOMove(rPoint, nodes[currentNode].timetoPoint);
        for(int i = 0; i< nodes[currentNode].events.Count; i++)
        {
            StartCoroutine(ActivateEvent(i));
        }
        while(Vector3.Distance(CameraManager.cam.transform.position, rPoint) >= 0.01f)
        {
            yield return null;
        }
        yield return new WaitForSeconds(nodes[currentNode].timeAtPoint);
        currentNode++;
        if (currentNode < nodes.Count) StartCoroutine(MoveToPoint());
    }
    public void AddNode()
    {
        nodes.Add(new Node());
    }
    public void RemoveNode()
    {
        nodes.RemoveAt(nodes.Count - 1);
    }
    [System.Serializable]
    public class Node
    {
        public Transform point;
        public float timetoPoint;
        public float timeAtPoint;
        public List<NodeEvent> events;
    }
    [System.Serializable]
    public class NodeEvent
    {
        public int number;
        public float time;
    }
}
