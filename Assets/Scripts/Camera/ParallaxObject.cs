﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxObject : MonoBehaviour
{
    public float velocity;
    [HideInInspector]
    public Vector2 initialLocalPos;
	// Use this for initialization
	void Start ()
    {
        transform.SetParent(CameraManager.cam.parallax.transform);
        CameraManager.cam.parallax.AddParallaxObject(this);
        initialLocalPos = transform.localPosition;
	}
}
