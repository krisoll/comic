﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
    public static CameraManager cam;
    public ParallaxComponent parallax;
    public Vector3 camOffset;
	// Use this for initialization
	void Awake ()
    {
        if (cam != null) Destroy(this.gameObject);
        cam = this;
        parallax.camOffset = camOffset;
	}
}
