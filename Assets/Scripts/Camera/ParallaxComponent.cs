﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxComponent : MonoBehaviour
{
    public List<ParallaxObject> parallaxObject;
    [HideInInspector]
    public Vector3 camOffset;
    private Vector3 distance;
    private Vector2 startPosition;
    private void Start()
    {
        startPosition = transform.position;
    }
    public void AddParallaxObject(ParallaxObject pax)
    {
        parallaxObject.Add(pax);
    }
    private void Update()
    {
        distance = (Vector2)transform.position - startPosition;
        MoveObjects();
    }
    void MoveObjects()
    {
        for(int i = 0; i < parallaxObject.Count; i++)
        {
            parallaxObject[i].transform.localPosition = (distance) * -parallaxObject[i].velocity + 
                                                    camOffset + (Vector3)parallaxObject[i].initialLocalPos;
        }
    }
}
